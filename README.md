# STEAM APP           #
This project was created to acquaint our team with the angular framework during the internship in the EPAM, the instructions for use are described below.
___
## ***Instruction***
___
To get started, run this project on your computer or PC and write next points into your terminal:  npm install (to download dependencies), npm run build  and then npm start, and open it on a local host 5000,
then you need to sing in to your account. 



The project is created in such a way that the data is extracted from the mokap file, so if there is no your mail and password, sing in will not work. You can write "admin@gmail.com" with password "admin" to try our project.


After singing in, you can customize your profile by adding a username and age.

You also have the ability to add and delete friends that are also stored in the mockup file, select games, sort them by price and genre, and add to the library, where you can then download or share them with friends.
___

If you want to see our deploy, you can click [here](https://steam-app-clone.herokuapp.com).

