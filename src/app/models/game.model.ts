export interface Game {
  id: number,
  name: string,
  price: number,
  genre: string,
  img: string,
  description: string
}