export enum Path {
  AUTH = 'auth',
  GAMES = 'games',
  LIBRARY = 'library',
  FRIENDS = 'friends',
  PROFILE = 'profile'
}

export enum UserObjectProperty {
  EMAIL = 'email',
  PASSWORD = 'password',
  NAME = 'name',
  AGE = 'age'
}

export enum Message {
  SAVE_DATA = 'Your data was successfully saved!',
  ADD_FRIEND = 'Friend was successfully added!',
  REMOVE_FRIEND = `Friend was successfully removed!`,
  ADD_GAME = 'Game was successfully added!',
  START_DOWNLOAD = 'Download successfully started!',
  SHARE_GAME = 'You successfully shared the game!'
}